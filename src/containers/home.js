import React from "react"
import { GiPin } from 'react-icons/gi';
import Form from '../components/form';
import List from '../components/list';

const Home = (props) =>  {
    return (
        <div>
        <header>
        <h1>Pin It</h1>
        <p>A simple way to keep track of all the things that need doing and of the things that you've done</p>
        </header>
        <Form setToDo={props.setToDo} createToDo={props.createToDo} toDo={props.toDo}/>
        <List toDos={props.toDos} deleteToDo={props.deleteToDo} completedToDo={props.completedToDo} pinnedToDo={props.pinnedToDo}/>
        </div>
    )
}

export default Home