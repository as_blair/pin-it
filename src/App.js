import React, {useState,useEffect} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom'
import Home from './containers/home'
import About from './containers/about'
import Navbar from './components/navbar'

import './App.css';

function App() {

  const [toDo, setToDo ] = useState({toDo: '', completed: false, pinned: false})
  const [toDos, setToDos] = useState([])

  useEffect(()=>{
    const localToDos = JSON.parse(localStorage.getItem('toDos'));
    localToDos === null
    // create an array of todos in locSto in case it doesn t exist
    ? localStorage.setItem('toDos', JSON.stringify([]))
    : setToDos([...localToDos])
  },[])

  
  const createToDo = (e) => {
    e.preventDefault()
    if(toDo.toDo === '')return alert('no data provided')
    const temp = toDos
    temp.push(toDo)
    localStorage.setItem('toDos', JSON.stringify(temp))
    setToDos(temp)
    setToDo({toDo: '', completed: false})
    console.log(toDos)
  }
  
  const deleteToDo = (idx) => {
    const temp = toDos
    temp.splice(idx, 1)
    localStorage.setItem('toDos', JSON.stringify(temp))
    setToDos([...temp])
  }

  const loopThrough = () => {
    var elements = {
        initial: [],
        completed: [],
        pinned: []
    }
    toDos.forEach( todo => {
      if(todo.completed){
         elements.completed.push(todo)
      }else if(todo.pinned){
         elements.pinned.push(todo)
      }else{
         elements.initial.push(todo)
      }
    })
   
    return [...elements.pinned,...elements.initial,...elements.completed]
  }
  
  const completedToDo = (idx) => {
    const temp = toDos 
    temp[idx].completed = !temp[idx].completed
    temp[idx].pinned = false
    // return loopThrough()
    const test = loopThrough()
    localStorage.setItem('toDos', JSON.stringify(test))
        setToDos([...test])
    
}
var pinnedToDo = (idx) => {
  const temp = toDos
    temp[idx].pinned = !temp[idx].pinned
    temp[idx].completed = false
    const test = loopThrough()
    localStorage.setItem('toDos', JSON.stringify(test))
      setToDos([...test])
}

  


  // const pinnedToDo = (idx) => {
  //   const temp = toDos
  //   const tempPinned = []
  //   temp[idx].pinned = !temp[idx].pinned
  //   temp.map((item, idx) => {
  //     if(item.pinned) {
  //       tempPinned.unshift(item)
  //     } else {
  //             tempPinned.push(item)
  //     }
  //   })
  //     localStorage.setItem('toDos', JSON.stringify(tempPinned))
  //     setToDos([...tempPinned])
  // }
    
  

  return (
    <div>
      <Router>
          <Navbar />
          <Route exact path="/"      render={(props) => <Home setToDo={setToDo} 
                  createToDo={createToDo} toDo={toDo} toDos={toDos} deleteToDo={deleteToDo} 
                  completedToDo={completedToDo} pinnedToDo={pinnedToDo}/>}  />
          <Route exact path="/about" component={About} />
      </Router>
      
      
    </div>
  );
}

export default App;
