import React, {useState} from 'react';
import { MdDeleteForever } from "react-icons/md";
import { GiPin } from 'react-icons/gi';
import { GoCheck } from "react-icons/go";

const getStyles = (pinned, completed) => {
    
    if(pinned){
        return ('pinned')
    }
    if(completed){
        return ('checked')
    }
    return null
}


export default function List(props) {

return     props.toDos.map((item, idx) => {
                return <div className='listWrapper' key ={idx}>
                    <div>
                        <div className={`listStyle ${getStyles(item.pinned, item.completed)}` }>{item.toDo}</div>
                    </div>
                    <div className='actions'>
                    <button className='buttonStyle' onClick={() => props.pinnedToDo(idx)}><GiPin /></button>
                    <button className='buttonStyle' onClick={() => props.completedToDo(idx)}><GoCheck /></button>    
                        {/* <input className='checkboxStyle' type='checkbox' checked={item.pinned} 
                            onChange={() => props.pinnedToDo(idx)}/> */}
                        {/* <input className='checkboxStyle' type='checkbox' checked={item.completed} 
                            onChange={() => props.completedToDo(idx)}/> */}
                        <button className='deleteStyle' onClick={() => props.deleteToDo(idx)} ><MdDeleteForever /></button>
                    </div>
                </div>
            })
    
}