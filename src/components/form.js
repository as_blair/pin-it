import React, {useState} from 'react';
import { GiPin } from 'react-icons/gi';

export default function Form(props) {

    return <div className='wrapper'>
            <form className='formStyle' onSubmit={props.createToDo}>
                <input className='inputStyle' placeholder=' Add your to do here' onChange={(e) => props.setToDo({toDo: e.target.value, completed: false})}
                value = {props.toDo.toDo}/>
               
            </form>
        </div>
        
    
}